import java.io.*;

public class Solution
{
	/**
	 * Fill in the blocked region described into the given table.
	 * @param table - the table to fill the blocked in region for.
	 * @param rowBlock1 - row (0-based indexed) of upper left corner of blocked region.
	 * @param columnBlock1 - column (0-based indexed) of upper left corner of blocked region.
	 * @param rowBlock2 - row (0-based indexed) of bottom right corner of blocked region.
	 * @param columnBlock2 - column (0-based indexed) of bottom right corner of blocked region.
	 */
	public static void fillBlockedRegion(Long[][] table, int rowBlock1, int columnBlock1, int rowBlock2, int columnBlock2)
	{
		for (int i = rowBlock1; i <= rowBlock2; i++)
		{
			for (int j = columnBlock1; j <= columnBlock2; j++)
			{
				table[i][j] = 0L;
			}
		}
	}


	/**
	 * Retrieve the number of paths to reach the given point from the point above.
	 * @param table - the table the given point is found on.
	 * @param row - the row (0-based indexed) of the point to get the number of paths coming from above for.
	 * @param column - the column (0-based indexed) of the point to get the number of paths coming from above for.
	 * @return the number of paths to reach the given point by coming from the point above the given point.
	 */
	public static Long getNumberOfPathsFromAbove(Long[][] table, int row, int column)
	{
		int rowAbove = row - 1;
		if (rowAbove < 0)
		{
			return 0L;
		}
		else
		{
			return table[rowAbove][column];
		}
	}


	/**
	 * Retrieve the number of paths to reach the given point from the point left.
	 * @param table - the table the given point is found on.
	 * @param row - the row (0-based indexed) of the point to get the number of paths coming from the left.
	 * @param column - the column (0-based indexed) of the point to get the number of paths coming from the left.
	 * @return the number of paths to reach the given point by coming from the point left of the given point.
	 */
	public static Long getNumberOfPathsFromLeft(Long[][] table, int row, int column)
	{
		int columnLeft = column - 1;
		if (columnLeft < 0)
		{
			return 0L;
		}
		else
		{
			return table[row][columnLeft];
		}
	}


	/**
	 * Determine the number of paths from the upper left corner to the bottom right corner of the given table.
	 * @param table - the table to find the total number of paths from the upper left to the bottom right.
	 * @return paths from the upper left corner to the bottom right corner of the given table
	 */
	public static Long getNumberOfPaths(Long[][] table)
	{
		// For loop to iterate through all the rows.
		for (int i = 0; i < table.length; i++)
		{
			// For loop to iterate through all the columns.
			for (int j = 0; j < table[0].length; j++)
			{
				if (table[i][j] == null)
				{
					if (i == 0 && j == 0)
					{
						table[i][j] = 1L;
					}
					else
					{
						Long numberOfPathsFromAbove = getNumberOfPathsFromAbove(table, i, j);
						Long numberOfPathsFromLeft = getNumberOfPathsFromLeft(table, i, j);
						table[i][j] = numberOfPathsFromAbove + numberOfPathsFromLeft;
					}
				}
			}
		}

		return table[table.length - 1][table[0].length - 1];
	}


	public static void main(String[] args) throws NumberFormatException, IOException
	{
		String filePath = "C:\\Users\\Richard\\Desktop\\test.input1.txt";
		FileInputStream inputFile = new FileInputStream(new File(filePath));
		BufferedReader fileStream = new BufferedReader(new InputStreamReader(inputFile));

		int numberOfTables = Integer.parseInt(fileStream.readLine());
		while (numberOfTables > 0)
		{
			String[] tableDimensions = fileStream.readLine().split(" ");
			int numberOfRows = Integer.parseInt(tableDimensions[0]);
			int numberOfColumns = Integer.parseInt(tableDimensions[1]);
			String[] blockedRectangle = fileStream.readLine().split(" ");
			int rowBlock1 = Integer.parseInt(blockedRectangle[0]);
			int columnBlock1 = Integer.parseInt(blockedRectangle[1]);
			int rowBlock2 = Integer.parseInt(blockedRectangle[2]);
			int columnBlock2 = Integer.parseInt(blockedRectangle[3]);
			Long[][] table = new Long[numberOfRows][numberOfColumns];
			fillBlockedRegion(table, rowBlock1 - 1, columnBlock1 - 1, rowBlock2 - 1, columnBlock2 - 1);
			Long numberOfPaths = getNumberOfPaths(table);
			// Print out the total number of paths modulo by (10^9 + 7) as requested in the challenge.
			System.out.println((int) (numberOfPaths % (Math.pow(10, 9) + 7)));
			numberOfTables--;
		}
	}
}
