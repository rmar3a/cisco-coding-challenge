import java.io.*;


public class Solution
{
	/**
	 * Determine the (index) value of the given base 32 symbol.
	 * @param base32Symbol - the base 32 symbol to find the (index) value of.
	 * @return the (index) value of the given base 32 symbol or 32 for pad symbols.
	 */
	public static int mapToBase32Value(char base32Symbol)
	{
		return "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567=".indexOf(base32Symbol);
	}


	/**
	 * Determine the base 64 symbol of the given (index) value.
	 * @param base64Value - the (index) value of the base 64 symbol to find.
	 * @return the base 64 symbol of the given (index) value.
	 */
	public static char mapToBase64Symbol(int base64Value)
	{
		return "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(base64Value);
	}


	/**
	 * Convert the given base 32 string to its equivalent binary string.
	 * @param base32String - the base 32 string to convert to binary.
	 * @return the binary representation (padding removed, i.e. no binary representation for padding) of
	 *         the given base 32 string.
	 */
	public static String convertToBinaryString(String base32String)
	{
		// For loop to convert each base 32 symbol into its binary equivalent.
		StringBuilder binaryStringBuilder = new StringBuilder();
		for (char symbol : base32String.toCharArray())
		{
			// Each symbol is contained within a set of 5 bits (except for padding, which is artifically
			// given the value of 32).
			int mappedValue = mapToBase32Value(symbol);
			if (mappedValue == 32)
			{
				break;
			}
			binaryStringBuilder.append(String.format("%5s", Integer.toBinaryString(mappedValue)));
		}
		return binaryStringBuilder.toString().replaceAll(" ", "0");
	}


	/**
	 * Determine the number of pad symbols "=" that need to be added to the base 64 string.
	 * @param lastBits - the last bits available after forming groups of 24 bits in the base 64 string.
	 * @return the number of pad symbols that need to be added to the base 64 string.
	 */
	public static int calculateNumberOfPadSymbolsRequired(String lastBits)
	{
		if (lastBits.length() == 24)
		{
			return 0;
		}

		String last24Bits = String.format("%-24s", lastBits).replaceAll(" ", "0");
		String byte1 = last24Bits.substring(0, 8);
		String byte2 = last24Bits.substring(8, 16);
		String byte3 = last24Bits.substring(16, 24);
		
		if (byte3.contains("1"))
		{
			return 0;
		}
		else if (byte2.contains("1"))
		{
			return 1;
		}
		else if (byte1.contains("1"))
		{
			return 2;
		}
		else
		{
			return 0;
		}
	}


	/**
	 * Convert the given base 32 string to its equivalent base 64.
	 * Note: As required by the challenge, no external libraries are allowed to be used for the encoding.
	 * @param base32String - the base 32 string to convert to base 64.
	 * @return the base 64 representation of the given base 32 string.
	 */
	public static String convertToBase64(String base32String)
	{
		String base32BinaryString = convertToBinaryString(base32String);
		
		// For loop to parse the binary string into a base 64 string without padding.
		StringBuilder base64StringBuilder = new StringBuilder();
		StringBuilder last24BitBuilder = new StringBuilder();
		StringBuilder sextetStringBuilder = new StringBuilder();
		for (char bit : base32BinaryString.toCharArray())
		{
			// Each symbol is contained within a set of 6 bits (except for padding).
			last24BitBuilder.append(bit);
			sextetStringBuilder.append(bit);
			if (sextetStringBuilder.length() == 6)
			{
				int base64Value = Integer.parseInt(sextetStringBuilder.toString(), 2);
				base64StringBuilder.append(mapToBase64Symbol(base64Value));
				sextetStringBuilder = new StringBuilder();
			}

			if (last24BitBuilder.length() == 24)
			{
				last24BitBuilder = new StringBuilder();
			}
		}

		// Process any trailing bits if they contain a "1", adding "0"s so that a sextet of bits is formed.
		if (sextetStringBuilder.length() > 0 && sextetStringBuilder.indexOf("1") != -1)
		{
			String trailingBits = String.format("%-6s", sextetStringBuilder.toString());
			String trailingBitsPadded = trailingBits.replaceAll(" ", "0");
			int base64Value = Integer.parseInt(trailingBitsPadded, 2);
			base64StringBuilder.append(mapToBase64Symbol(base64Value));
		}

		// Add any padding that is required to fill the 24-bit alignment requirement.
		// Note: 4 base 64 symbols fit in 24 bits.
		if (last24BitBuilder.length() > 0)
		{
			int numberOfPadSymbolsToAdd = calculateNumberOfPadSymbolsRequired(last24BitBuilder.toString());
			
			while (numberOfPadSymbolsToAdd > 0)
			{
				base64StringBuilder.append("=");
				numberOfPadSymbolsToAdd--;
			}
		}

		return base64StringBuilder.toString();
	}

	public static void main(String[] args) throws NumberFormatException, IOException
	{
		String filePath = "C:\\Users\\Richard\\Desktop\\test.input1.txt";
		FileInputStream inputFile = new FileInputStream(new File(filePath));
		BufferedReader fileStream = new BufferedReader(new InputStreamReader(inputFile));

		int numberOfLines = Integer.parseInt(fileStream.readLine());
		
		while (numberOfLines > 0)
		{
			String base32String = fileStream.readLine();
			System.out.println(convertToBase64(base32String));
			numberOfLines--;
		}
	}
}
