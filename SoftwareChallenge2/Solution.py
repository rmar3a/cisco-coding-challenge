"""
Cisco Software Challenge 2.

"""
import fileinput

QUERY_ALL = 1
QUERY_ANY = 2
QUERY_SOME = 3


def generateFrequencyCounts(list_of_numbers):
    """
    Function to create a dictionary containing the frequency counts of the numbers in the given list.
    @param list_of_numbers - list of numbers to gather the frequency counts for.
    @return a dictionary containing the frequency counts of the numbers in the file.

    """
    frequency_counts = {}
    for number in list_of_numbers:
        number = int(number)
        if number in frequency_counts:
            frequency_counts[number] += 1
        else:
            frequency_counts[number] = 1

    return frequency_counts


def applyQueryAll(file_list, query_frequencies):
    """
    Determine which of the given files contains all the values in the query.
    @param file_list - the list of files to query.
    @param query_frequencies - frequency count of the values in the query. 
    @return the number of files that match the query.

    """
    file_count = 0
    for file in file_list:
        for number in query_frequencies:
            if number not in file:
                break
            elif file[number] < query_frequencies[number]:
                break
        else:
            file_count += 1

    return file_count


def applyQueryAny(file_list, query_frequencies):
    """
    Determine which of the given files contains any of the values in the query.
    @param file_list - the list of files to query.
    @param query_frequencies - frequency count of the values in the query. 
    @return the number of files that match the query.

    """
    file_count = 0
    for file in file_list:
        for number in query_frequencies:
            if number in file:
                file_count += 1
                break

    return file_count


def applyQuerySome(file_list, query_frequencies):
    """
    Determine which of the given files contains some (but not all) the values in the query.
    @param file_list - the list of files to query.
    @param query_frequencies - frequency count of the values in the query. 
    @return the number of files that match the query.

    """
    file_count = 0
    for file in file_list:
        match_count = 0
        for number in query_frequencies:
            if number in file:
                match_count += 1
        total_count = len(file)
        if total_count != match_count and match_count != 0:
            file_count += 1

    return file_count


def processQuery(file_list, query):
    """
    Process the given query on the given list of files.
    @param file_list - list of dictionaries containing the frequency counts of the numbers for each file.
    @param query - the query to apply to the given list of files.
    @return the result of applying the given query on the given list of files.

    """
    query_type = int(query[0])
    numbers_of_interest = query[2:]
    query_frequencies = generateFrequencyCounts(numbers_of_interest)

    if query_type == QUERY_ALL:
        return applyQueryAll(file_list, query_frequencies)
    elif query_type == QUERY_ANY:
        return applyQueryAny(file_list, query_frequencies)
    else:
        return applyQuerySome(file_list, query_frequencies)


def main():
    with fileinput.input(files="C:\\Users\\Richard\\Desktop\\test.input1.txt") as filestream:
        number_of_files = int(filestream.readline())
        file_list = []

        # While loop to gather the frequency counts for each file.
        while number_of_files > 0:
            file_list.append(generateFrequencyCounts(filestream.readline().strip().split(" ")))
            number_of_files -= 1

        number_of_queries = int(filestream.readline())

        # While loop to process each query on the list of files.
        while number_of_queries > 0:
            print(processQuery(file_list, filestream.readline().strip().split(" ")))
            number_of_queries -= 1

if __name__ == '__main__':
    main()
