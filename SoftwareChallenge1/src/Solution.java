import java.io.*;


public class Solution
{
	public static final long MIN_REQUIRED_NUMBER_OF_PASSWORDS = 1000000L;

	/**
	 * Calculate the total number of passwords possible with the given length.
	 * Note: Valid characters are 0-9.
	 * @param passwordLength - the length of the password to create.
	 * @return total number of passwords with the given length.
	 */
	public static long getNumberOfPasswordsPossible(int passwordLength)
	{
		return (long)(Math.pow(10, passwordLength));
	}


	/**
	 * Determine if the given password system is secure.
	 * @param minPasswordLength - the length of the shortest valid password.
	 * @param maxPasswordLength - the length of the longest valid password.
	 * @param minRequiredNumberOfPasswords - the total number of passwords possible for the given password system should be more
	 *                                       than this value.
	 * @return "YES" if the password system is secure, otherwise "NO".
	 */
	public static String isSecurePasswordSystem(int minPasswordLength, int maxPasswordLength, long minRequiredNumberOfPasswords)
	{
		Long totalPasswordsSum = 0L;
		int currentPasswordLength = maxPasswordLength;
		while (currentPasswordLength >= minPasswordLength)
		{
			totalPasswordsSum += getNumberOfPasswordsPossible(currentPasswordLength);
			if (totalPasswordsSum > minRequiredNumberOfPasswords)
			{
				return "YES";
			}

			currentPasswordLength--;
		}

		return "NO";
	}


	public static void main(String[] args) throws NumberFormatException, IOException
	{
		String filePath = "C:\\Users\\Richard\\Desktop\\test.input1.txt";
		FileInputStream inputFile = new FileInputStream(new File(filePath));
		BufferedReader fileStream = new BufferedReader(new InputStreamReader(inputFile));

		int numberOfPasswordSystems = Integer.parseInt(fileStream.readLine());

		while (numberOfPasswordSystems > 0)
		{
			String[] passwordSystem = fileStream.readLine().split(" ");
			int passwordMinLength = Integer.parseInt(passwordSystem[0]);
			int passwordMaxLength = Integer.parseInt(passwordSystem[1]);
			System.out.println(isSecurePasswordSystem(passwordMinLength, passwordMaxLength, MIN_REQUIRED_NUMBER_OF_PASSWORDS));
			numberOfPasswordSystems--;
		}
	}
}
