import java.io.*;


public class Solution
{
	public static final int HEX_BASE = 16;

	/**
	 * Convert the given ASCII string into its equivalent Hex values.
	 * @param asciiString - the string to convert into an equivalent Hex value.
	 * @return the given string as its equivalent Hex value.
	 */
	public static String convertAsciiToHex(String asciiString)
	{
		// For loop to build up Hex string.
		StringBuilder hexStringBuilder = new StringBuilder();
		for (int i = 0; i < asciiString.length(); i++)
		{
			hexStringBuilder.append(Integer.toHexString(asciiString.charAt(i)));
		}

		return hexStringBuilder.toString();
	}


	/**
	 * Convert the given Hex valued string into its equivalent ASCII string.
	 * @param hexString - the string to convert into an equivalent ASCII value.
	 * @return the given string as its equivalent Hex value.
	 */
	public static String convertHexToAscii(String hexString)
	{
		// For loop to build up Hex string.
		StringBuilder asciiStringBuilder = new StringBuilder();
		for (int i = 0; i < hexString.length() - 1; i += 2)
		{
			String hexValue = String.valueOf(hexString.charAt(i)) + String.valueOf(hexString.charAt(i+1));
			int intValue = Integer.parseInt(hexValue, HEX_BASE);
			asciiStringBuilder.append((char)intValue);
		}

		return asciiStringBuilder.toString();
	}


	/**
	 * XOR the two given Hex valued strings.
	 * @param hexString1 - one of the Hex valued strings to XOR.
	 * @param hexString2 - other Hex valued strings to XOR.
	 * @return the result of XORing the two given Hex valued strings.
	 */
	public static String xorHexStringWithKey(String hexKey, String hexString)
	{
		hexKey = hexKey.toUpperCase();
		hexString = hexString.toUpperCase();

		// For loop to XOR each Hex value.
		StringBuilder xoredHexStringBuilder = new StringBuilder();
		for (int i = 0; i < hexString.length(); i++)
		{
			int hexKeyValueAsInt = Integer.parseInt(String.valueOf(hexKey.charAt(i % hexKey.length())), HEX_BASE);
			int hexStringValueAsInt = Integer.parseInt(String.valueOf(hexString.charAt(i)), HEX_BASE);
			xoredHexStringBuilder.append(Integer.toHexString(hexKeyValueAsInt ^ hexStringValueAsInt));
		}

		return xoredHexStringBuilder.toString();
	}


	public static void main(String[] args) throws IOException
	{
		String filePath = "C:\\Users\\Richard\\Desktop\\test.input1.txt";
		FileInputStream inputFile = new FileInputStream(new File(filePath));
		BufferedReader fileStream = new BufferedReader(new InputStreamReader(inputFile));

		String key = null;
		String encryptedMessage = null;

		key = fileStream.readLine();
		encryptedMessage = fileStream.readLine();

		String keyInHex = convertAsciiToHex(key);

		String xoredMessage = xorHexStringWithKey(keyInHex, encryptedMessage);
		String decryptedMessage = convertHexToAscii(xoredMessage);

		System.out.println(decryptedMessage);
	}
}
