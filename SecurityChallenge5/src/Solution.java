import java.io.*;

public class Solution
{

	/**
	 * Recursive function to determine if the given string is palindromic.
	 * @param inputString - the string to process.
	 * @return true if the given string is palindromic; otherwise false.
	 */
	public static boolean isPalindromic(String inputString)
	{
		if (inputString.length() == 1)
		{
			return true;
		}
		else
		{
			char firstCharacter = inputString.charAt(0);
			char lastCharacter = inputString.charAt(inputString.length() - 1);
			if (firstCharacter != lastCharacter)
			{
				return false;
			}
			else if (inputString.length() == 2)
			{
				return true;
			}
			else
			{
				return isPalindromic(inputString.substring(1, inputString.length() - 1));
			}
		}
	}


	/**
	 * Determine what the largest palindromic sequence in the given string is.
	 * @param inputString - the string to process.
	 * @return the largest palindromic sequence found in the given string.
	 */
	public static String getLargestPalindromicSubstring(String inputString)
	{
		// While loop to determine what window size to consider.
		int stringStart = 0;
		int stringEnd = inputString.length();
		int windowSize = stringEnd - stringStart;
		while (windowSize > 0)
		{
			// For loop to check substrings of the given window size.
			for (int i = 0; i + windowSize <= stringEnd; i++)
			{
				String subString = inputString.substring(i, i + windowSize);
				if (isPalindromic(subString))
				{
					return subString;
				}
			}
			windowSize--;
		}

		return "";
	}

	public static void main(String[] args) throws NumberFormatException, IOException
	{
		String filePath = "C:\\Users\\Richard\\Desktop\\test.input1.txt";
		FileInputStream inputFile = new FileInputStream(new File(filePath));
		BufferedReader fileStream = new BufferedReader(new InputStreamReader(inputFile));

		String inputString = fileStream.readLine();

		System.out.println(getLargestPalindromicSubstring(inputString));
	}
}
